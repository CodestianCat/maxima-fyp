import Vue from 'vue'
import Router from 'vue-router'
import firebase from "firebase";

import Home from './views/Home.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/index.html',
      redirect: '/'
    },
    {
      path: '/history',
      name: 'history',
      meta: { layout: "default" },
      component: () => import(/* webpackChunkName: "about" */ './views/History.vue')
    },
    {
      path: '/',
      name: 'topics',
      meta: { layout: "default" },
      component: () => import(/* webpackChunkName: "about" */ './views/Topics.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: { layout: "empty" },
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    }
  ]
})

router.beforeEach(async (to, from, next) => {

  try {
    await firebase.auth().onAuthStateChanged(user => {
      if (user) {
        if(to.path === '/login') {
          next('/');
        }
        else {
          next();
        }
      } else {
        if (to.path !== '/login') {
          next('/login')
        }
        else {
          if(to.path === '/login') {
            next();
          }
        }
      }
    });
  }
  catch (err) {
    console.log(err)
  }
  // firebase.auth().onAuthStateChanged((user) => {
  //   if (user) {
  //     console.log('LOGGED IN');
  //   } else {
  //     console.log('LOGGED OUT');
  //     if(to.path !== '/login') {
  //       next('/login')
  //     }
  //     console.log(to)
  //   }
  // });

})

export default router;