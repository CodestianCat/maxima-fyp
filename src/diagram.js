/* eslint-disable */
import * as THREE from 'three';
import { OrbitControls, MapControls } from 'three/examples/jsm/controls/OrbitControls';
import triangle from './renders/2D/2D_triangle';
import cuboid from './renders/3D/3D_cuboid';
import cylinder from './renders/3D/3D_cylinder';

import rectangle from './renders/2D/2D_rectangle';
import linear_equation from './renders/graph/graph_linear_equation';
const Parser = require('expr-eval').Parser;

export default class Diagram {
    constructor() {
        this.element,
            this.renderer,
            this.scene,
            this.renderer,
            this.camera,
            this.controls,
            this.requestId = undefined
    }
    init(mode, topic, config) {
        this.element = document.querySelector('#sample');
        this.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
        this.renderer.setSize(this.element.offsetWidth, this.element.offsetHeight);
        this.element.appendChild(this.renderer.domElement);
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(70, this.element.offsetWidth / this.element.offsetHeight, 0.1, 1000);


        if (mode == '3D') {
            this.camera.position.set(1, 1, 1.75);

            function light(x, y, z, scene) {
                const light = new THREE.DirectionalLight(0x009688, 0.5);
                light.position.set(x, y, z)
                scene.add(light)
            }

            light(-1, 2, 4, this.scene)
            light(1, -1, -2, this.scene)

            this.controls3D();

            switch (topic) {
                case "volume_cuboid":
                    const test = cuboid(config);
                    this.scene.add(test.shape)
                    this.scene.add(test.label)
                    break;
                default:
                    break;
            }

            // const test1 = cylinder();
            // this.scene.add(test1.shape)

        }
        else if (mode == '2D') {

            this.camera.up.set(1, 0, 0);

            this.controls2D();

            // const grid = new THREE.GridHelper(100, 100, 0x000000, 0x4d4d4d);
            // grid.material.linewidth = 2;
            // grid.material.transparent = true;
            // grid.material.opacity = 0.2;
            // this.scene.add(grid);

            switch (topic) {
                case "perimeter_triangle":
                    break;
                case "area_rectangle":
                    const rect = rectangle(config);
                    this.scene.add(rect.shape);
                    this.scene.add(rect.label);
                    this.scene.add(rect.dimension);
                    break;
                case "perimeter_rectangle":
                    const rect1 = rectangle(config);
                    this.scene.add(rect1.shape);
                    this.scene.add(rect1.label);
                    this.scene.add(rect1.dimension);
                    break;
                default:
                    break;
            }

            // const triangle = this.newtriangle([
            //     { label: 'D', x: 4, y: 7 },
            //     { label: 'M', x: -1, y: 2 },
            //     { label: 'Z', x: 8, y: 5 },
            // ]);

            // const triangle1 = triangle([
            //     { label: 'D', x: 12, z: -3, },
            //     { label: 'M', x: 8, z: 6 },
            //     { label: 'Z', x: 2, z: -7 },
            // ]);

            // this.scene.add(triangle1.shape);
            // this.scene.add(triangle1.label);
            // this.scene.add(triangle.angle);

            this.setCenter2D(0, 0);
        }
        else if (mode == 'graph') {
            this.camera.up.set(1, 0, 0);

            this.controls2D();
            this.setCenter2D(0, 0);

            // const grid = new THREE.GridHelper(100, 100, 0x000000, 0x4d4d4d);
            // grid.material.linewidth = 2;
            // grid.material.transparent = true;
            // grid.material.opacity = 0.2;
            // this.scene.add(grid);

            const arr1 = [];
            const arr2 = [];
            const arr3 = [];

            for (let i = -50; i < 50 + 1; i++) {
                if (Number.isInteger(i / 5) && i != 0) {
                    arr1.push(-50, 0, i);
                    arr1.push(50, 0, i);
                    arr1.push(i, 0, -50);
                    arr1.push(i, 0, 50);
                }
                else if (i === 0) {
                    arr3.push(-50, 0, i);
                    arr3.push(50, 0, i);
                    arr3.push(i, 0, -50);
                    arr3.push(i, 0, 50);
                }
                else {
                    arr2.push(-50, 0, i);
                    arr2.push(50, 0, i);
                    arr2.push(i, 0, -50);
                    arr2.push(i, 0, 50);
                }
            }

            var vertices1 = new Float32Array(arr1);

            var geometry1 = new THREE.BufferGeometry();
            geometry1.setAttribute('position', new THREE.BufferAttribute(vertices1, 3));

            var material1 = new THREE.LineBasicMaterial({ color: 0x000000, linewidth: 2, transparent: true, opacity: 0.2 });
            var mesh1 = new THREE.LineSegments(geometry1, material1);

            this.scene.add(mesh1);

            var vertices2 = new Float32Array(arr2);

            var geometry2 = new THREE.BufferGeometry();
            geometry2.setAttribute('position', new THREE.BufferAttribute(vertices2, 3));

            var material2 = new THREE.LineBasicMaterial({ color: 0x000000, linewidth: 2, transparent: true, opacity: 0.1 });
            var mesh2 = new THREE.LineSegments(geometry2, material2);

            this.scene.add(mesh2);

            var vertices3 = new Float32Array(arr3);

            var geometry3 = new THREE.BufferGeometry();
            geometry3.setAttribute('position', new THREE.BufferAttribute(vertices3, 3));

            var material3 = new THREE.LineBasicMaterial({ color: 0x000000, linewidth: 2, transparent: true, opacity: 0.4 });
            var mesh3 = new THREE.LineSegments(geometry3, material3);

            this.scene.add(mesh3);


            var geometry = new THREE.BufferGeometry();
            // create a simple square shape. We duplicate the top left and bottom right
            // vertices because each vertex needs to appear once per triangle.
            const arr = [];

            const parser = new Parser();

            var expr = parser.parse('3 * x^2 + 6 * x + 32');

            const SLICES = 50;

            const MIN = -50;
            const MAX = 50;

            for (let i = (MIN) * 10; i < (MAX + 1) * 10; i++) {

                if (expr.evaluate({ x: i / 10 }) < MIN || expr.evaluate({ x: i / 10 }) > MAX || Number.isNaN(expr.evaluate({ x: i / 10 }))) {
                    continue;
                }

                console.log(expr.evaluate({ x: i / 10 }));

                arr.push(expr.evaluate({ x: i / 10 }))
                arr.push(0.01)
                arr.push(i / 10)
            }

            var vertices = new Float32Array(arr);

            // itemSize = 3 because there are 3 values (components) per vertex
            geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
            var material = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
            var mesh = new THREE.Line(geometry, material);

            this.scene.add(mesh);


            // this.scene.add(linear_equation());
        }
        this.animate();

    }
    remove() {
        this.stopAnimate();
        this.element.removeChild(this.element.childNodes[0]);
    }
    animate() {
        this.requestId = undefined;
        // requestAnimationFrame(this.animate.bind(this));
        this.controls.update();
        this.renderer.render(this.scene, this.camera);

        this.startAnimate();
    }
    startAnimate() {
        if (!this.requestId) {
            this.requestId = requestAnimationFrame(this.animate.bind(this));
        }
    }
    stopAnimate() {
        if (this.requestId) {
            cancelAnimationFrame(this.requestId);
            this.requestId = undefined;
        }
    }
    onWindowResize() {
        this.camera.aspect = this.element.offsetWidth / this.element.offsetHeight;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(this.element.offsetWidth, this.element.offsetHeight);
    }
    controls3D() {
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enablePan = false;
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.5;

        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.2;
        this.controls.maxPolarAngle = Math.PI / 2;

        this.controls.maxDistance = 20;
        this.controls.minDistance = 15;
    }
    controls2D() {
        this.controls = new MapControls(this.camera, this.renderer.domElement);
        this.controls.screenSpacePanning = true;
        this.controls.enableRotate = false;
        this.controls.enableDamping = true;
        this.controls.maxDistance = 30;
        this.controls.minDistance = 5;
        this.controls.dampingFactor = 0.5;

    }
    setCenter2D(x, y) {
        this.controls.target.set(x, 0, y);
        this.camera.position.lerp({ x: x, y: 0, z: y }, 1);
        this.camera.position.y = 12;

    }
    sphere() {
        const geometry = new THREE.SphereGeometry(5, 32, 32);
        const material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
        const sphere = new THREE.Mesh(geometry, material);
        return sphere;
    }
}

// var sphereGeom = new THREE.SphereGeometry(1, 30, 30);
// var blueMaterial = new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.2 });
// var sphere = new THREE.Mesh(sphereGeom, blueMaterial);

// this.scene.add(sphere)

// const material = new THREE.LineDashedMaterial({
//     color: 0xffffff,
//     linewidth: 2,
//     dashSize: 0.05,
//     gapSize: 0.05
// });

// var geometry = new THREE.Geometry();
// var geometry1 = new THREE.Geometry();
// geometry.vertices.push(
//     new THREE.Vector3(0, 1, 0),
//     new THREE.Vector3(0, 0, 0),
//     new THREE.Vector3(0, 0, 1)
// );

// geometry1.vertices.push(
//     new THREE.Vector3(0, 0, 0),
//     new THREE.Vector3(1, 0, 0)
// );

// var line = new THREE.Line(geometry, material);
// var line1 = new THREE.Line(geometry1, material);

// line.computeLineDistances();
// line1.computeLineDistances();

// this.scene.add(line)
// this.scene.add(line1)