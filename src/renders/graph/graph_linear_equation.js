import * as THREE from 'three';

export default function linear_equation() {
    const c = Math.floor(Math.random() * 6) -10;
    const m = 0.5;

    const x1 = (50 - c) / m;
    const x2 = (-50 - c) / m;

    const material = new THREE.LineBasicMaterial({
        color: 0xffffff
    });

    const geometry = new THREE.Geometry();
    geometry.vertices.push(
        new THREE.Vector3(50, 0.001, x1),
        new THREE.Vector3(-50, 0.001, x2)
    );

    return new THREE.Line(geometry, material);
}

