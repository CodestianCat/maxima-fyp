import * as THREE from "three";
import SpriteText from "../others/Sprite_Text";

// export default class Cylinder {
//     constructor() {
//          this.geometry = new THREE.Geometry();
//          this.label = new THREE.Group();
//          this.dimension = new THREE.Group();
//          this.angle = new THREE.Group();
//     }
// }

export default function cylinder(r, h) {

    const geometry = new THREE.CircleGeometry(0.75, 50);
    const material = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
    geometry.vertices.shift();
    const circle = new THREE.LineLoop(geometry, material);
    circle.position.y = 0.76;
    circle.rotation.x = - Math.PI / 2;

    const geometry1 = new THREE.CylinderGeometry(0.75, 0.75, 1.5, 50);
    const material1 = new THREE.MeshPhongMaterial({ color: 0x00877A, transparent: true, opacity: 0.25 });
    const cylinder = new THREE.Mesh(geometry1, material1);

    const text = SpriteText('5 cm', 0, 0, -Math.PI / 2);
    text.position.x = 0.9;

    const shape = new THREE.Group();
    shape.add(cylinder);

    shape.add(text);

    return { shape };
}