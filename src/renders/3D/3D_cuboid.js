import * as THREE from "three";
import SpriteText from "../others/Sprite_Text";

export default function cuboid(config) {

    let big_no, x, y, z;

    big_no = Math.max(...config.length);

    x = config.length[0]/big_no * 10;
    y = config.length[1]/big_no * 10;
    z = config.length[2]/big_no * 10;

    const geometry = new THREE.BoxGeometry(x, y, z);
    var material = new THREE.MeshPhongMaterial({color: 0xffffff, transparent: true, opacity: 0.25 });

    var shape = new THREE.Mesh(geometry, material);
    const label = new THREE.Group();

    const test = SpriteText(config.points[0], 0, 0, 0);
    test.position.x = (x/2) + 0.4;
    test.position.y = (y/2) + 0.4;
    test.position.z = (z/2) + 0.4;

    const test1 = SpriteText(config.points[1], 0, 0, 0);
    test1.position.x = -(x/2) - 0.4;
    test1.position.y = (y/2) + 0.4;
    test1.position.z = (z/2) + 0.4;

    const test2 = SpriteText(config.points[2], 0, 0, 0);
    test2.position.x = -(x/2) - 0.4;
    test2.position.y = -(y/2) - 0.4;
    test2.position.z = (z/2) + 0.4;

    const test3 = SpriteText(config.points[3], 0, 0, 0);
    test3.position.x = (x/2) + 0.4;
    test3.position.y = -(y/2) - 0.4;
    test3.position.z = (z/2) + 0.4;

    const test4 = SpriteText(config.points[7], 0, 0, 0);
    test4.position.x = (x/2) + 0.4;
    test4.position.y = (y/2) + 0.4;
    test4.position.z = -(z/2) - 0.4;

    const test5 = SpriteText(config.points[6], 0, 0, 0);
    test5.position.x = -(x/2) - 0.4;
    test5.position.y = (y/2) + 0.4;
    test5.position.z = -(z/2) + -0.4;

    const test6 = SpriteText(config.points[5], 0, 0, 0);
    test6.position.x = -(x/2) - 0.4;
    test6.position.y = -(y/2) - 0.4;
    test6.position.z = -(z/2) + -0.4;

    const test7 = SpriteText(config.points[4], 0, 0, 0);
    test7.position.x = (x/2) + 0.4;
    test7.position.y = -(y/2) - 0.4;
    test7.position.z = -(z/2) + -0.4;

    const ttest4 = SpriteText(config.length[0] + ' cm', 0, 0, 0);
    ttest4.position.x = 0;
    ttest4.position.y = -(y/2) - 0.4;
    ttest4.position.z = (z/2) + 0.4;

    const ttest5 = SpriteText(config.length[2] + ' cm', 0, 0, 0);
    ttest5.position.x = (x/2) + 0.4;
    ttest5.position.y = -(y/2) - 0.4;
    ttest5.position.z = 0;

    const ttest6 = SpriteText(config.length[1] + ' cm', 0, 0, -Math.PI / 2);
    ttest6.position.x = (x/2) + 0.4;
    ttest6.position.y = 0;
    ttest6.position.z = (z/2) + 0.4;

    label.add(test);
    label.add(test1);
    label.add(test2);
    label.add(test3);

    label.add(test4);
    label.add(test5);
    label.add(test6);
    label.add(test7);

    label.add(ttest4);
    label.add(ttest5);
    label.add(ttest6);

    return { shape, label };
}