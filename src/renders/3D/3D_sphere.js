import * as THREE from "three";

export default class Sphere {
    constructor() {
         this.geometry = new THREE.Geometry();
         this.label = new THREE.Group();
         this.dimension = new THREE.Group();
         this.angle = new THREE.Group();
    }
}
