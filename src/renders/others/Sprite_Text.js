import { SpriteMaterial, Texture, LinearFilter, Sprite } from "three";

export default function SpriteText(text, x, y, rotation) {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    canvas.height = 300;

    context.font = "30px Arial";
    context.fillStyle = "#ffffff";
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillText(text, 150, 150);

    const texture = new Texture(canvas);
    texture.needsUpdate = true;
    texture.minFilter = LinearFilter;

    const spriteMaterial = new SpriteMaterial({ map: texture, rotation, depthWrite: false });
    const sprite = new Sprite(spriteMaterial);
    sprite.position.x = x;
    sprite.position.z = y;
    sprite.scale.set(6, 6, 1)

    return sprite;
}