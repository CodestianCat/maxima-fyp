import * as THREE from "three";
import SpriteText from "../others/Sprite_Text";

function makeAngle(x, y, top, down) {
    const curve = new THREE.EllipseCurve(
        x, -y,            // ax, aY
        1, 1,           // xRadius, yRadius
        top, 0,  // aStartAngle, aEndAngle
        true,            // aClockwise
        down               // aRotation
    );
    const points = curve.getPoints(50);

    const geometry = new THREE.BufferGeometry().setFromPoints(points);
    const material = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
    const ellipse = new THREE.Line(geometry, material);

    ellipse.rotation.x = - Math.PI / 2;

    return ellipse;
}

export default function triangle(config) {

    var geometry = new THREE.Geometry();

    geometry.vertices.push(new THREE.Vector3(config[0].z, 0.01, config[0].x));
    geometry.vertices.push(new THREE.Vector3(config[1].z, 0.01, config[1].x));
    geometry.vertices.push(new THREE.Vector3(config[2].z, 0.01, config[2].x));
    geometry.vertices.push(new THREE.Vector3(config[0].z, 0.01, config[0].x));
    const material = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });

    const shape = new THREE.Line(geometry, material);
    const label = new THREE.Group();
    const dimension = new THREE.Group();
    const angle = new THREE.Group();

    config.forEach((element, index) => {

        let mid_x, mid_z, offset_x, offset_z, dAx, dAy, dBx, dBy, angle1, length0, length1;

        switch (index) {
            case 0:
                mid_x = (config[1].x + config[2].x) / 2;
                mid_z = (config[1].z + config[2].z) / 2;

                dAx = config[1].x - config[0].x;
                dAy = config[1].z - config[0].z;
                dBx = config[2].x - config[0].x;
                dBy = config[2].z - config[0].z;
                angle1 = Math.atan2(dAx * dBy - dAy * dBx, dAx * dBx + dAy * dBy);
                if (angle1 < 0) { angle1 = angle1 * -1; }

                length0 = Math.abs(config[1].x - config[0].x)
                length1 = Math.abs(config[1].z - config[0].z)

                offsetangle = Math.atan(length1 / length0);
                if (offsetangle < 0) { offsetangle = offsetangle * -1; }

                if ((config[0].x > config[1].x) && (config[0].z > config[1].z)) {
                    console.log("BOTTOM LEFT");
                    offsetangle += Math.PI / 2
                }
                else if((config[0].x < config[1].x) && (config[0].z < config[1].z)) {
                    console.log("TOP RIGHT");
                    offsetangle += Math.PI / 2 * 3;
                }
                else if((config[0].x > config[1].x) && (config[0].z < config[1].z)) {
                    console.log("TOP LEFT");
                    offsetangle += 0;
                }
                else if((config[0].x < config[1].x) && (config[0].z > config[1].z)) {
                    console.log("BOTTOM RIGHT");
                    offsetangle += Math.PI;
                    // offsetangle += Math.PI - angle1/2;
                }

                angle.add(makeAngle(config[0].z, config[0].x, angle1, offsetangle))

                break;
            case 1:
                mid_x = (config[0].x + config[2].x) / 2;
                mid_z = (config[0].z + config[2].z) / 2;

                dAx = config[0].x - config[1].x;
                dAy = config[0].z - config[1].z;
                dBx = config[2].x - config[1].x;
                dBy = config[2].z - config[1].z;
                angle1 = Math.atan2(dAx * dBy - dAy * dBx, dAx * dBx + dAy * dBy);
                if (angle1 < 0) { angle1 = angle1 * -1; }

                length0 = Math.abs(config[2].x - config[1].x)
                length1 = Math.abs(config[2].z - config[1].z)

                let offsetangle = Math.atan(length1 / length0);
                if (offsetangle < 0) { offsetangle = offsetangle * -1; }


                if ((config[1].x > config[2].x) && (config[1].z > config[2].z)) {
                    console.log("BOTTOM LEFT");
                    offsetangle += Math.PI / 2
                }
                else if((config[1].x < config[2].x) && (config[1].z < config[2].z)) {
                    console.log("TOP RIGHT");
                    offsetangle += Math.PI / 2 * 3;
                }
                else if((config[1].x > config[2].x) && (config[1].z < config[2].z)) {
                    console.log("TOP LEFT");
                    offsetangle += 0;
                }
                else if((config[1].x < config[2].x) && (config[1].z > config[2].z)) {
                    console.log("BOTTOM RIGHT");
                    offsetangle += Math.PI;
                }

                angle.add(makeAngle(config[1].z, config[1].x, angle1, offsetangle))

                break;
            case 2:
                mid_x = (config[0].x + config[1].x) / 2;
                mid_z = (config[0].z + config[1].z) / 2;

                dAx = config[0].x - config[2].x;
                dAy = config[0].z - config[2].z;
                dBx = config[1].x - config[2].x;
                dBy = config[1].z - config[2].z;
                angle1 = Math.atan2(dAx * dBy - dAy * dBx, dAx * dBx + dAy * dBy);
                if (angle1 < 0) { angle1 = angle1 * -1; }

                length0 = Math.abs(config[0].x - config[2].x)
                length1 = Math.abs(config[0].z - config[2].z)

                offsetangle = Math.atan(length0 / length1);
                if (offsetangle < 0) { offsetangle = offsetangle * -1; }

                if ((config[2].x > config[0].x) && (config[2].z > config[0].z)) {
                    console.log("BOTTOM LEFT");
                    offsetangle += Math.PI / 2
                }
                else if((config[2].x < config[0].x) && (config[2].z < config[0].z)) {
                    console.log("TOP RIGHT");
                    offsetangle += Math.PI / 2 * 3;
                }
                else if((config[2].x > config[0].x) && (config[2].z < config[0].z)) {
                    console.log("TOP LEFT");
                    offsetangle += 0;
                }
                else if((config[2].x < config[0].x) && (config[2].z > config[0].z)) {
                    console.log("BOTTOM RIGHT");
                    offsetangle += Math.PI;
                }

                angle.add(makeAngle(config[2].z, config[2].x, angle1, offsetangle))

                break;
        }


        if (config[index].x > mid_x) {
            // console.log('MOVE LEFT')
            offset_x = 0.75;
        }
        else if (config[index].x < mid_x) {
            // console.log('MOVE RIGHT')
            offset_x = -0.75;
        }
        else if (config[index].x == mid_x) {
            offset_x = 0;
        }

        if (config[index].z > mid_z) {
            // console.log('Left')
            offset_z = 0.75;
        }
        else if (config[index].z < mid_z) {
            // console.log('Right')
            offset_z = -0.75;
        }

        label.add(SpriteText(element.label, element.z + offset_z, element.x + offset_x, 0))
    });

    shape.matrixAutoUpdate = false;
    label.matrixAutoUpdate = false;
    dimension.matrixAutoUpdate = false;

    const centroid = {
        x: (config[0].x+config[1].x+config[2].x)/3,
        z: (config[0].z+config[1].z+config[2].z)/3
    };


    return { shape, label, dimension, angle, centroid };
}
