import { Geometry, Vector3, LineBasicMaterial, Line, Group} from "three";
import SpriteText from "../others/Sprite_Text";

export default function rectangle(config) {

    let big_no, x, y;

    big_no = Math.max(...config.length);

    // if(config.length[0] > config.length[1]) {big_no = config.length[0]}
    // else {big_no = config.length[1]}

    y = config.length[1]/big_no * 5;
    x = config.length[0]/big_no * 5;

    var geometry = new Geometry();
    const label = new Group();
    const dimension = new Group();

    geometry.vertices.push(new Vector3(y, 0.01, - x));
    geometry.vertices.push(new Vector3(- y, 0.01, - x));
    geometry.vertices.push(new Vector3(- y, 0.01, x));
    geometry.vertices.push(new Vector3(y, 0.01, x));
    geometry.vertices.push(new Vector3(y, 0.01, - x));

    const material = new LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
    const shape = new Line(geometry, material);

    label.add(SpriteText(config.points[0], y + 0.7, -x - 0.7, 0));
    label.add(SpriteText(config.points[1], y + 0.7, x + 0.7, 0));
    label.add(SpriteText(config.points[2], -y - 0.7, x + 0.7, 0));
    label.add(SpriteText(config.points[3], -y  -0.7, -x - 0.7, 0));

    dimension.add(SpriteText(config.length[0] + ' cm', y + 0.75, 0, 0));
    dimension.add(SpriteText(config.length[1] + ' cm', 0, x + 0.75, - Math.PI / 2));

    shape.matrixAutoUpdate = false;
    label.matrixAutoUpdate = false;
    dimension.matrixAutoUpdate = false;

    return { shape, label, dimension };
}