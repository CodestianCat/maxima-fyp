import * as THREE from "three";

export function circle(radius, x, y) {

    const geometry = new THREE.CircleGeometry(radius, 64);
    const material = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });
    const circle = new THREE.LineLoop(geometry, material);

    geometry.vertices.shift();

    circle.rotation.x = - Math.PI / 2;
    circle.position.x = x;
    circle.position.z = y;
    circle.position.y = 0.01;

    return circle;

}