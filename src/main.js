import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as firebase from "firebase";

import axios from 'axios';
import Vuelidate from 'vuelidate'

import Default from './layouts/Default'
import Empty from './layouts/Empty'

import './registerServiceWorker'
import vuetify from './plugins/vuetify';

Vue.prototype.$http = axios;
Vue.use(Vuelidate)

Vue.component('default-layout', Default);
Vue.component('empty-layout', Empty);

Vue.config.productionTip = false

const configOptions = {
  apiKey: "AIzaSyClHlZGowyPu4jKlG58u8dC1_J6iG1ofTA",
  authDomain: "maxidus-71764.firebaseapp.com",
  databaseURL: "https://maxidus-71764.firebaseio.com",
  projectId: "maxidus-71764",
  storageBucket: "maxidus-71764.appspot.com",
  messagingSenderId: "897234514560",
  appId: "1:897234514560:web:12905bb908f3b166e22c65",
  measurementId: "G-P4G11RP524"
};

firebase.initializeApp(configOptions);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');
