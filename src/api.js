const URL = 'http://localhost:3000';

const api_auth = {
    login: URL + '/auth/login/',
    register: URL + '/auth/register/'
};

const api_problems = {
    addition: URL + '/math/addition/',
    subtraction: URL + '/math/subtraction/',
    multiplication: URL + '/math/multiplication/',
    division: URL + '/math/division/',
};

export { api_auth, api_problems };