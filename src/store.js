import Vue from 'vue'
import Vuex from 'vuex'
import Diagram from './diagram'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    problem: {},
    problemTopic: '',
    problemDisplay: false,
    problemBackground: 'grey',
    engine: new Diagram(),
    renderType: 'none',
    history: []
  },
  mutations: {
    changeProblemDisplay(state) {
      state.problemDisplay = !state.problemDisplay;
    },
    changeProblemTopic(state, topic) {
      state.problemTopic = topic;
    },
    changeProblemBackground(state, color) {
      state.problemBackground = color;
    },
    changeProblem(state, problem) {
      state.problem = problem;
    },
    addHistory(state, problem) {
      state.history.unshift(problem);
    },
    changeRenderType(state, type) {
      state.renderType = type;
    }
  },
  actions: {
    changeProblemDisplay(context) {
      context.commit('changeProblemDisplay');
    },
    changeProblemTopic(context, payload) {
      context.commit('changeProblemTopic', payload.topic)
    },
    changeProblemBackground(context, payload) {
      context.commit('changeProblemBackground', payload.color);
    },
    changeProblem(context, payload) {
      context.commit('changeProblem', payload);
    },
    addHistory(context, payload) {
      context.commit('addHistory', payload);
    },
    changeRenderType(context, payload) {
      context.commit('changeRenderType', payload);
    }
  }
})
