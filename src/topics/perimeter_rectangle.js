export default function perimeter_rectangle(level) {

    let solution = 0;
    let problem = '';
    let numarr = [];
    const alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
    let points = [];

    for (let i = 0; i < 4; i++) {
        const no = Math.floor(Math.random() * (25 - i));
        points.push(alphabets[no]);
        alphabets.splice(no, 1);
    }

    switch (level) {
        case 1:
            for (let i = 0; i < 2; i++) {
                let number = Math.floor(Math.random() * 19) + 5;
                numarr.push(number);
            }
            break;
        case 2:
            for (let i = 0; i < 2; i++) {
                let number = Math.floor(Math.random() * 199) + 10;
                numarr.push(number);
            }
            break;
    }

    problem = 'Find the perimeter of rectangle ' + points.toString().replace(/,/g, '');

    solution = numarr[0] + numarr[0] + numarr[1] +numarr[1];

    return { problem, solution, points, length: numarr };
}