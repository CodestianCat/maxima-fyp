import addition from "./addition";
import subtraction from "./subtraction";
import multiplication from "./multiplication";
import area_rectangle from "./area_rectangle";
import perimeter_rectangle from "./perimeter_rectangle";
import volume_cuboid from "./volume_cuboid";

export default {addition, subtraction, multiplication,area_rectangle,perimeter_rectangle,volume_cuboid};