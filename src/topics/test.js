const alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
let points = [];


for (let i = 0; i < 4; i++) {
    const no = Math.floor(Math.random() * (25 - i));
    points.push(alphabets[no]);
    alphabets.splice(no,1);
}