export default function addition(level) {

    let solution = 0;
    let problem = '';
    let numarr = [];

    switch (level) {
        case 1:
            for (let i = 0; i < 2; i++) {
                let number = Math.floor(Math.random() * 9) + 1;
                numarr.push(number);
                if (i === 0) { problem = problem.concat(`${number}`) }
                else { problem = problem.concat(` + ${number}`) }
            }
            break;
        case 2:
            for (let i = 0; i < (Math.floor(Math.random() * 3) + 2); i++) {
                let number = Math.floor(Math.random() * 9) + 1;
                numarr.push(number);
                if (i === 0) { problem = problem.concat(`${number}`) }
                else { problem = problem.concat(` + ${number}`) }
            }
            break;
        case 3:
            for (let i = 0; i < 2; i++) {
                let number = Math.floor(Math.random() * 89) + 5;
                numarr.push(number);
                if (i === 0) { problem = problem.concat(`${number}`) }
                else { problem = problem.concat(` + ${number}`) }
            }
            break;
        case 4:
            for (let i = 0; i < (Math.floor(Math.random() * 3) + 2); i++) {
                let number = Math.floor(Math.random() * 89) + 5;
                numarr.push(number);
                if (i === 0) { problem = problem.concat(`${number}`) }
                else { problem = problem.concat(` + ${number}`) }
            }
            break;
        case 5:
            for (let i = 0; i < 2; i++) {
                let number = Math.floor(Math.random() * 899) + 100;
                numarr.push(number);
                if (i === 0) { problem = problem.concat(`${number}`) }
                else { problem = problem.concat(` + ${number}`) }
            }
            break;
    }

    solution = numarr.reduce((a, b) => a + b);

    return { problem, solution };
}